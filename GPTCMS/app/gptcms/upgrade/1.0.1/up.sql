
-- ----------------------------
-- Table structure for `kt_gptcms_aliai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_aliai_config`;
CREATE TABLE `kt_gptcms_aliai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `accesskey_id` varchar(255) DEFAULT NULL,
  `accesskey_secret` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `appkey` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_aliai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_baiduai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_baiduai_config`;
CREATE TABLE `kt_gptcms_baiduai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `appid` int(11) DEFAULT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  `secretkey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_baiduai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_card`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_card`;
CREATE TABLE `kt_gptcms_card` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) DEFAULT NULL,
  `type` int(10) DEFAULT NULL COMMENT '1为对话次，3为vip时长',
  `size` int(10) DEFAULT NULL COMMENT '与类型为对应',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `amount` int(10) DEFAULT NULL COMMENT '生成卡密数量',
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_card
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_card_detail`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_card_detail`;
CREATE TABLE `kt_gptcms_card_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '卡密',
  `time` datetime DEFAULT NULL COMMENT '使用时间',
  `user` int(10) DEFAULT NULL COMMENT '使用者',
  `status` int(10) DEFAULT '0' COMMENT '0未使用，1为已使用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_card_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_chat_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_chat_msg`;
CREATE TABLE `kt_gptcms_chat_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `group_id` int(11) DEFAULT '0',
  `un_message` text COMMENT '用户发出内容(未过滤)',
  `message` text COMMENT '用户发出内容(已过滤)',
  `un_response` text COMMENT '助手回复内容(未过滤)',
  `response` text COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出+回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_chat_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_chat_msg_group`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_chat_msg_group`;
CREATE TABLE `kt_gptcms_chat_msg_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '客户端用户id',
  `name` varchar(255) DEFAULT NULL COMMENT '分组名称',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_chat_msg_group
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_chatmodel_set`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_chatmodel_set`;
CREATE TABLE `kt_gptcms_chatmodel_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '1开启  0关闭',
  `gpt35` text COMMENT 'GPT3.5',
  `gpt4` text COMMENT 'Gpt4',
  `linkerai` text COMMENT '灵犀星火',
  `api2d35` text COMMENT 'api2d3.5',
  `api2d4` varchar(255) DEFAULT NULL COMMENT 'api2d4',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='对话模型设置';

-- ----------------------------
-- Records of kt_gptcms_chatmodel_set
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_cmodel`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_cmodel`;
CREATE TABLE `kt_gptcms_cmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `tp_url` varchar(255) DEFAULT NULL COMMENT '图片url',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `bz` varchar(255) DEFAULT NULL COMMENT '备注',
  `xh` int(11) DEFAULT '0' COMMENT '序号 越大越靠前',
  `vip_status` tinyint(1) DEFAULT '0' COMMENT 'vip是否可使用 1可以使用 0不能使用',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1正常 0暂停使用',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `classify_id` int(5) DEFAULT NULL COMMENT '分类id',
  `content` text COMMENT '模型内容',
  `hint_content` text COMMENT '对话框提示文字',
  `defalut_question` text COMMENT '默认问题',
  `defalut_reply` text COMMENT '默认回复',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='创作模型';

-- ----------------------------
-- Records of kt_gptcms_cmodel
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_cmodel_classify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_cmodel_classify`;
CREATE TABLE `kt_gptcms_cmodel_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_cmodel_classify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_common_user`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_common_user`;
CREATE TABLE `kt_gptcms_common_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '系统用户id',
  `type` varchar(50) DEFAULT NULL COMMENT 'xcx 微信小程序 h5 H5 wx 微信',
  `parent` int(11) DEFAULT '0' COMMENT '所属上级',
  `level` int(11) DEFAULT '1' COMMENT '级别',
  `residue_degree` int(11) DEFAULT '0' COMMENT '剩余条数',
  `vip_expire` datetime DEFAULT NULL COMMENT 'vip到期时间',
  `vip_open` datetime DEFAULT NULL COMMENT 'vip开通时间',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `headimgurl` varchar(255) DEFAULT NULL COMMENT '头像',
  `account` varchar(255) DEFAULT NULL COMMENT '账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `unionid` varchar(255) DEFAULT NULL COMMENT '开放平台id',
  `token` varchar(255) DEFAULT NULL COMMENT 'token',
  `expire_time` int(11) DEFAULT NULL COMMENT 'token过期时间',
  `xcx_token` varchar(255) DEFAULT NULL COMMENT '微信小程序token',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1正常 0停用',
  `bz` varchar(255) DEFAULT NULL COMMENT '备注',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_common_user
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_content_security`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_content_security`;
CREATE TABLE `kt_gptcms_content_security` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `question_baiduai` tinyint(1) DEFAULT '1' COMMENT '提问审核 百度ai文本审核开关 1开启 0关闭',
  `reply_baiduai` tinyint(1) DEFAULT '1' COMMENT '回复审核 百度ai文本审核开关  1开启 0关闭',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='内容安全设置';

-- ----------------------------
-- Records of kt_gptcms_content_security
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_create_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_create_msg`;
CREATE TABLE `kt_gptcms_create_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `model_id` int(11) DEFAULT NULL COMMENT '创作模型id',
  `un_message` text COMMENT '用户发出内容(未过滤)',
  `message` text COMMENT '用户发出内容(已过滤)',
  `make_message` text COMMENT '最终组合提交接口的内容',
  `un_response` text COMMENT '助手回复内容(未过滤)',
  `response` text COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出+回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_create_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_gpt_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_gpt_config`;
CREATE TABLE `kt_gptcms_gpt_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `channel` tinyint(1) DEFAULT '1' COMMENT '渠道1.openai 2.api2d 3.文心一言 4.通义千问 5.昆仑天工 6.ChatGLM',
  `openai` text COMMENT 'openai',
  `api2d` text COMMENT 'api2d',
  `wxyy` text COMMENT '文心一言配置',
  `tyqw` text COMMENT '通义千文',
  `kltg` text COMMENT '昆仑天工',
  `chatglm` text COMMENT 'chatglm',
  `linkerai` text COMMENT '灵犀星火',
  `gpt4` text,
  `api2d4` text,
  `u_time` datetime DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_gpt_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_gptpaint_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_gptpaint_config`;
CREATE TABLE `kt_gptcms_gptpaint_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `channel` tinyint(1) DEFAULT '1' COMMENT '渠道1 意间',
  `yjai` text COMMENT '意间AI',
  `u_time` datetime DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  `replicate` text,
  `status` tinyint(1) DEFAULT '1' COMMENT '是否开启绘画 1开启  0关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_gptpaint_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_h5_wx`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_h5_wx`;
CREATE TABLE `kt_gptcms_h5_wx` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '页面标题',
  `share_tile` varchar(255) DEFAULT NULL COMMENT '分享标题',
  `share_desc` varchar(255) DEFAULT NULL COMMENT '分享描述',
  `share_image` varchar(255) DEFAULT NULL COMMENT '分享图片链接',
  `status` tinyint(1) DEFAULT '0' COMMENT '微信登陆 1开启 0关闭',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_h5_wx
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_hot`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_hot`;
CREATE TABLE `kt_gptcms_hot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `content` text COMMENT '内容',
  `sort` int(11) DEFAULT '0' COMMENT '越大越靠前',
  `classify_id` int(11) DEFAULT NULL COMMENT '分类id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_hot
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_hot_classify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_hot_classify`;
CREATE TABLE `kt_gptcms_hot_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='热门分类';

-- ----------------------------
-- Records of kt_gptcms_hot_classify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_invite_award`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_invite_award`;
CREATE TABLE `kt_gptcms_invite_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT ' 1开启 0关闭',
  `number` int(11) DEFAULT NULL COMMENT '邀请一次奖励数量',
  `up_limit` int(11) DEFAULT NULL COMMENT '邀请人数上限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='邀请奖励';

-- ----------------------------
-- Records of kt_gptcms_invite_award
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_jmodel`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_jmodel`;
CREATE TABLE `kt_gptcms_jmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `tp_url` varchar(255) DEFAULT NULL COMMENT '图片url',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `bz` varchar(255) DEFAULT NULL COMMENT '备注',
  `xh` int(11) DEFAULT '0' COMMENT '序号 越大越靠前',
  `vip_status` tinyint(1) DEFAULT '0' COMMENT 'vip是否可使用 1可以使用 0不能使用',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1正常 0暂停使用',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `classify_id` int(5) DEFAULT NULL COMMENT '分类id',
  `content` text COMMENT '模型内容',
  `hint_content` text COMMENT '对话框提示文字',
  `defalut_question` text COMMENT '默认问题',
  `defalut_reply` text COMMENT '默认回复',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色模型';

-- ----------------------------
-- Records of kt_gptcms_jmodel
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_jmodel_classify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_jmodel_classify`;
CREATE TABLE `kt_gptcms_jmodel_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色模型分类';

-- ----------------------------
-- Records of kt_gptcms_jmodel_classify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_miniprogram`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_miniprogram`;
CREATE TABLE `kt_gptcms_miniprogram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `appid` varchar(255) DEFAULT NULL,
  `appsecret` varchar(255) DEFAULT NULL,
  `mch_id` varchar(255) DEFAULT NULL,
  `mch_key` varchar(255) DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_miniprogram
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_paint_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_paint_msg`;
CREATE TABLE `kt_gptcms_paint_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `un_message` text CHARACTER SET utf16 COMMENT '用户发出内容(未过滤)',
  `message` text COMMENT '用户发出内容(已过滤)',
  `un_response` text COMMENT '助手回复内容(未过滤)',
  `response` text COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出 回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0生成失败 1处理中 2处理成功',
  `u_time` int(11) DEFAULT NULL,
  `sync_status` tinyint(1) DEFAULT '0' COMMENT '1已同步 0未同步',
  `chatmodel` varchar(255) DEFAULT NULL COMMENT '渠道',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_paint_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_paintmodel_set`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_paintmodel_set`;
CREATE TABLE `kt_gptcms_paintmodel_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `sd` text COMMENT '灵犀星火 sd绘画',
  `yjai` text COMMENT '意间Ai',
  `gpt35` text COMMENT 'openai 绘画',
  `api2d35` text COMMENT 'api2d绘画',
  `replicate` text COMMENT 'replicateMJ',
  `status` tinyint(1) DEFAULT '0' COMMENT '1开启 0关闭',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='绘画模型设置';

-- ----------------------------
-- Records of kt_gptcms_paintmodel_set
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_paintmsg_notify`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_paintmsg_notify`;
CREATE TABLE `kt_gptcms_paintmsg_notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `task_id` varchar(255) DEFAULT NULL COMMENT '回调任务di',
  `chatmodel` varchar(255) DEFAULT NULL COMMENT '渠道',
  `msgid` int(11) DEFAULT NULL COMMENT '消息id',
  `c_time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0未回调  1已回调',
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_paintmsg_notify
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_pay`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_pay`;
CREATE TABLE `kt_gptcms_pay` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `out_trade_no` char(50) DEFAULT NULL COMMENT '微信自定义订单号',
  `orderid` int(11) DEFAULT NULL COMMENT '订单id',
  `order_bh` char(50) DEFAULT NULL COMMENT '订单编号',
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `common_id` int(11) DEFAULT NULL COMMENT '用户id',
  `uip` char(50) DEFAULT NULL COMMENT '请求ip',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `status` char(50) DEFAULT NULL COMMENT '状态',
  `alipayzt` char(50) DEFAULT NULL,
  `bz` varchar(250) DEFAULT NULL,
  `ifok` int(11) DEFAULT NULL COMMENT '是否支付 1:已支付  0:未支付',
  `wxddbh` char(50) DEFAULT NULL COMMENT '微信订单编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  `jyh` varchar(255) DEFAULT NULL COMMENT '交易单号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_pay
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_pay_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_pay_config`;
CREATE TABLE `kt_gptcms_pay_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `config` text COMMENT '支付配置',
  `type` varchar(100) DEFAULT NULL COMMENT '类型: wx 微信  ali 支付宝',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='支付配置';

-- ----------------------------
-- Records of kt_gptcms_pay_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_pay_order`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_pay_order`;
CREATE TABLE `kt_gptcms_pay_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `common_id` int(11) DEFAULT NULL,
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `order_bh` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `status` tinyint(1) DEFAULT '1' COMMENT '1待支付 2支付成功 3取消支付',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `type` tinyint(1) DEFAULT '9' COMMENT '1天 2周 3月 4季度  5年 9条',
  `pay_type` varchar(255) DEFAULT NULL COMMENT 'wx 微信   alipay支付宝',
  `ddbh` varchar(255) DEFAULT NULL COMMENT '微信订单号',
  `setmeal_type` varchar(255) DEFAULT NULL COMMENT 'vip VIP套餐  recharge充值套餐',
  `setmeal_id` int(11) DEFAULT NULL COMMENT '套餐表id',
  `buy_number` int(11) DEFAULT NULL COMMENT '购买数量',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT '微信交易订单号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='支付订单记录';

-- ----------------------------
-- Records of kt_gptcms_pay_order
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_pc`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_pc`;
CREATE TABLE `kt_gptcms_pc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '页面标题',
  `bottom_desc` varchar(255) DEFAULT NULL COMMENT '底部声明',
  `desc_link` varchar(255) DEFAULT NULL COMMENT '生命链接',
  `status` tinyint(1) DEFAULT '0' COMMENT '微信登陆 1开启 0关闭',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_pc
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_qzzl`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_qzzl`;
CREATE TABLE `kt_gptcms_qzzl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `content` text,
  `u_time` datetime DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_qzzl
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_recharge_setmeal`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_recharge_setmeal`;
CREATE TABLE `kt_gptcms_recharge_setmeal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL COMMENT '序号  越大越靠前',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `old_price` decimal(10,2) DEFAULT NULL COMMENT '划线价',
  `bz` varchar(255) DEFAULT NULL,
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_recharge_setmeal
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_reward_record`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_reward_record`;
CREATE TABLE `kt_gptcms_reward_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `num` int(11) DEFAULT NULL COMMENT '奖励条数',
  `type` tinyint(1) DEFAULT '1' COMMENT '奖励类型 1注册 2登录 3邀请',
  `c_time` datetime DEFAULT NULL COMMENT '奖励时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_reward_record
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_role_msg`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_role_msg`;
CREATE TABLE `kt_gptcms_role_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `model_id` int(11) DEFAULT NULL COMMENT '角色模型id',
  `tip_message` text COMMENT '指令内容',
  `un_message` text COMMENT '用户发出内容(未过滤)',
  `message` text COMMENT '用户发出内容(已过滤)',
  `un_response` text COMMENT '助手回复内容(未过滤)',
  `response` text COMMENT '助手回复内容(已过滤)',
  `total_tokens` int(11) DEFAULT NULL COMMENT '发出+回复字符串的总长度',
  `c_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_role_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_share_award`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_share_award`;
CREATE TABLE `kt_gptcms_share_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT ' 1开启 0关闭',
  `number` int(11) DEFAULT NULL COMMENT '分享一次奖励数量',
  `up_limit` int(11) DEFAULT NULL COMMENT '分享数量上限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='分享奖励';

-- ----------------------------
-- Records of kt_gptcms_share_award
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_share_rewards`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_share_rewards`;
CREATE TABLE `kt_gptcms_share_rewards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '终端用户id',
  `num` int(11) DEFAULT NULL COMMENT '奖励条数',
  `c_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_share_rewards
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_sms_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_sms_config`;
CREATE TABLE `kt_gptcms_sms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `access_key_id` varchar(255) DEFAULT NULL COMMENT '阿里云短信key',
  `access_key_secret` varchar(255) DEFAULT NULL COMMENT '阿里云短信密钥',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='阿里云短信配置';

-- ----------------------------
-- Records of kt_gptcms_sms_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_sms_template`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_sms_template`;
CREATE TABLE `kt_gptcms_sms_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `bh` varchar(20) DEFAULT NULL,
  `sign_name` varchar(100) DEFAULT NULL,
  `template_code` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_sms_template
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_storage_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_storage_config`;
CREATE TABLE `kt_gptcms_storage_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '账户id',
  `type` tinyint(1) DEFAULT '2' COMMENT '2:oss阿里云 3: cos 腾讯云 4: kodo七牛云',
  `oss_id` varchar(100) DEFAULT NULL COMMENT '阿里云 id',
  `oss_secret` varchar(100) DEFAULT NULL COMMENT '阿里云 secret',
  `oss_endpoint` varchar(100) DEFAULT NULL COMMENT '阿里云  访问域名',
  `oss_bucket` varchar(100) DEFAULT NULL COMMENT '阿里云bucket',
  `cos_secretId` varchar(100) DEFAULT NULL COMMENT '腾讯云 id',
  `cos_secretKey` varchar(100) DEFAULT NULL COMMENT '腾讯云 key',
  `cos_bucket` varchar(100) DEFAULT NULL COMMENT '腾讯云 bucket',
  `cos_endpoint` varchar(100) DEFAULT NULL COMMENT '腾讯云 endpoint',
  `kodo_key` varchar(100) DEFAULT NULL COMMENT '七牛云 key',
  `kodo_secret` varchar(100) DEFAULT NULL COMMENT '七牛云 secret',
  `kodo_domain` varchar(100) DEFAULT NULL COMMENT '七牛云 domain',
  `kodo_bucket` varchar(100) DEFAULT NULL COMMENT '七牛云 bucket',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='云存储配置';

-- ----------------------------
-- Records of kt_gptcms_storage_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_system`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_system`;
CREATE TABLE `kt_gptcms_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `rz_number` int(11) DEFAULT '0' COMMENT '注册赠送次数',
  `dz_number` int(11) DEFAULT '0' COMMENT '每日赠送次数',
  `dz_remind` text COMMENT '超出每日赠送次数提醒',
  `zdz_number` int(11) DEFAULT NULL COMMENT '总每日赠送次数限制',
  `zdz_remind` varchar(255) DEFAULT NULL COMMENT '超出每日总次数提示语',
  `yq_number` int(11) DEFAULT '0' COMMENT '邀请奖励次数',
  `welcome` text COMMENT '欢迎语',
  `sms` tinyint(1) DEFAULT '0' COMMENT '短信开关 1开启 0关闭',
  `self_balance` varchar(20) DEFAULT NULL COMMENT '自定义余额',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_system
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_tencentai_config`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_tencentai_config`;
CREATE TABLE `kt_gptcms_tencentai_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `secret_id` varchar(255) DEFAULT NULL,
  `secret_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_tencentai_config
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_vad_award`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_vad_award`;
CREATE TABLE `kt_gptcms_vad_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT ' 1开启 0关闭',
  `number` int(11) DEFAULT NULL COMMENT '邀请一次奖励数量',
  `up_limit` int(11) DEFAULT NULL COMMENT '邀请人数上限',
  `ad_id` varchar(255) DEFAULT NULL COMMENT '广告位id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='观看广告奖励';

-- ----------------------------
-- Records of kt_gptcms_vad_award
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_vip_setmeal`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_vip_setmeal`;
CREATE TABLE `kt_gptcms_vip_setmeal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0' COMMENT '序号 越大越靠前',
  `duration` int(11) DEFAULT NULL COMMENT '时长',
  `duration_type` tinyint(1) DEFAULT NULL COMMENT '1天 2周 3月 4季度  5年',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `old_price` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_vip_setmeal
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_websit`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_websit`;
CREATE TABLE `kt_gptcms_websit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '站点标题',
  `sms` tinyint(1) DEFAULT '0',
  `kfcode` varchar(255) DEFAULT NULL COMMENT '客服二维码',
  `gzhcode` varchar(255) DEFAULT NULL COMMENT '公众号二维码',
  `pcwxlogin_status` tinyint(1) DEFAULT '0' COMMENT 'pc微信登录开关  1开启 0关闭',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_websit
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_wx_user`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_wx_user`;
CREATE TABLE `kt_gptcms_wx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '系统用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '关联基础用户id',
  `openid` varchar(255) DEFAULT NULL COMMENT 'openid',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `headimgurl` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` tinyint(1) DEFAULT '0' COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `city` varchar(255) DEFAULT NULL COMMENT '市',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  `unionid` varchar(255) DEFAULT NULL COMMENT '开放平台id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_wx_user
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_wxgzh`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_wxgzh`;
CREATE TABLE `kt_gptcms_wxgzh` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `wid` int(10) NOT NULL,
  `appid` varchar(255) DEFAULT NULL,
  `appsecret` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL COMMENT '令牌',
  `message_key` varchar(255) DEFAULT NULL COMMENT '消息加解密密钥',
  `message_mode` varchar(255) DEFAULT NULL COMMENT '明文加密',
  `type` tinyint(1) DEFAULT '1' COMMENT '1手动配置 2扫码授权',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_wxgzh
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_xcx`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_xcx`;
CREATE TABLE `kt_gptcms_xcx` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `share_image` varchar(255) DEFAULT NULL COMMENT '分享图片链接',
  `ios_status` tinyint(1) DEFAULT '0' COMMENT 'ios支付 1开启 0关闭',
  `xcx_audit` tinyint(1) DEFAULT NULL COMMENT '小程序审核模式 1开启 0关闭',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_xcx
-- ----------------------------

-- ----------------------------
-- Table structure for `kt_gptcms_xcx_user`
-- ----------------------------
DROP TABLE IF EXISTS `kt_gptcms_xcx_user`;
CREATE TABLE `kt_gptcms_xcx_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wid` int(11) DEFAULT NULL COMMENT '系统用户id',
  `common_id` int(11) DEFAULT NULL COMMENT '关联基础用户id',
  `openid` varchar(255) DEFAULT NULL COMMENT 'openid',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `headimgurl` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` tinyint(1) DEFAULT '1' COMMENT '性别 0女 1男',
  `city` varchar(255) DEFAULT NULL COMMENT '市',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  `unionid` varchar(255) DEFAULT NULL COMMENT '开放平台id',
  `c_time` datetime DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of kt_gptcms_xcx_user
-- ----------------------------
